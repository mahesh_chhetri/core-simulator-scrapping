import path from "path";
import fs from "fs";
import cheerio from "cheerio";
import DomFinder from "../../classes/DomFinder";


// fs.readFile(path.resolve('data/1#home.html'), 'utf-8',function (err,oldHtml) {
//     var oldHtml = oldHtml;
//     fs.readFile(path.resolve('data/1#home-copy.html'), 'utf-8',function (err,newHtml) {
//         var diff = new HtmlDiff(oldHtml, newHtml).build();
//                 fs.writeFile(path.resolve('diff/diff.html'), diff, function (err) {
//                     // do stuff afte written file
//                 });
//     });
fs.readFile(path.resolve("data/1#home.html"), "utf-8", function (err, html) {
  const $ = cheerio.load(html);
  let fileDom = {};

  let htmlElem = $("html");
  let bodyElem = $("body");
  // console.log("boddd", htmlElem);
  let htmlNode = new DomFinder().init(htmlElem);
  let body = new DomFinder().init(bodyElem);
  fileDom['htmlNode'] = htmlNode;
  fileDom['htmlNode']["body"] = body;
  console.log("data", fileDom);
});
