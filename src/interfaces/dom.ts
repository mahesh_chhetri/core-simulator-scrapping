interface IElement {
    tagName: string;
    language: string;
    parentNode: string;
    totalChildren: number;
    childElements: string[];
    nextElement: string;
  }