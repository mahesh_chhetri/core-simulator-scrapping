var NYC = NYC || {};

/**
 * Class NYC.ScrollToTop
 */
NYC.ScrollToTop = Class.extend({

    init : function init(options, elem) {

        this.options = $.extend({}, this.options, options);

        this.element = elem || $('#top-button');
        this.elementData = this.element.data('position');

        // a container that you want to watch for resize events
        // the height of this will affect where the button transitions from fixed to absolute position
        this.resizeTarget;
        if(this.options.resizeTarget) { this.resizeTarget = this.options.resizeTarget; }

        // containers
        this.myWin = $(window);
        this.innerWrap = $('#inner-wrap');
        this.myHeader = $('#top');
        this.myFooter = $('footer');
        this.myThreeOneOneFooter = $('div.module-threeoneonefooter');

        // dimensions
        this.myWinHeight = this.myWin.height();
        this.yTopOffset = parseInt(this.elementData.ytopoffset);
        this.yBottomOffset = parseInt(this.elementData.ybottomoffset);

        this.yTop = 0;
        this.yBottom = 0;
        this.absoluteYBottom = 0;
        this.setYBottomLimits();

        this.bindEvents();
    },

    options : {},

    bindEvents: function() {
        var self = this;

        // browser window resizing
        self.myWin.smartresize(function() {
            self.setYBottomLimits();
        });

        // element resizing - jquery.ba-resize plugin
        if(self.resizeTarget) {
            self.resizeTarget.resize(function() {
                self.setYBottomLimits();
            });
        }


        // scrolling
        self.myWin.scroll(function() {
            self.scrollTopTest();
        });

        //click handler
        this.element.click(function(e) {
            e.preventDefault();
            $('html, body').animate({ scrollTop: 0 }, 400);
        });

    }, //bindEvents

    setYBottomLimits : function() {
        var self = this;
        // height of the header + custom or default offset value
        self.yTop             = self.myHeader.height() + self.yTopOffset;
        //height of browser window
        self.myWinHeight      = self.myWin.height();
        // height of page content - height of browser window - footer containers heights - custom or default offset value
        self.yBottom          = self.innerWrap.height() - self.myWinHeight - self.myFooter.outerHeight() - self.myThreeOneOneFooter.outerHeight() - self.yBottomOffset;
        // height of page content - bottom limit - height of browser window + bottom offset + half the el's height + 5(?)
        self.absoluteYBottom  = self.innerWrap.height() - self.yBottom - self.myWinHeight + self.yBottomOffset + (self.element.height()/2) + 5;

        self.scrollTopTest();
    },

    scrollTopTest : function() {
        var self = this;
        //test whether it's the top limit, the bottom limit, or the goldilocks zone
        if(self.myWin.scrollTop() < self.yTop) {
            self.element.fadeOut(100);
        }
        else if(self.myWin.scrollTop() > self.yBottom ) {
            self.setAbsolutePosition();
        }
        else {
            self.setFixedPosition();
        }
    },

    setFixedPosition: function() {
        var self = this;
        switch(self.elementData.xorientation) {
            case 'left': self.element.css({ 'left': self.elementData["xfxd"] });
                break;
            case 'right': self.element.css({ 'right': self.elementData["xfxd"] });
                break;
        }
        switch(self.elementData.yorientation) {
            case 'top': self.element.css({ 'top': self.elementData["yfxd"] });
                break;
            case 'bottom': self.element.css({ 'bottom': self.elementData["yfxd"] });
                break;
        }
        //show it if it's hidden
        self.element.css({ 'position': 'fixed', 'visibility': 'visible' }).fadeIn(100);
    },

    setAbsolutePosition: function() {
        var self = this;
        switch(self.elementData.xorientation) {
            case 'left': self.element.css({ 'left': self.elementData["xabs"] });
                break;
            case 'right': self.element.css({ 'right': self.elementData["xabs"] });
                break;
        }
        switch(self.elementData.yorientation) {
            case 'top': self.element.css({ 'top': self.absoluteYBottom });  //self.elementData["yabs"]
                break;
            case 'bottom': self.element.css({ 'bottom': self.absoluteYBottom });  //self.elementData["yabs"]
                break;
        }
        //show it if it's hidden
        self.element.css({ 'position': 'absolute', 'visibility': 'visible' }).fadeIn(100);
    }


});
